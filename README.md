# weather_challenge_app

### Optional Tasks status description

**Not-Started** (Only reason was limited for not finishing the tasks was the limited time)
* UI tests using Espresso 
* Implement 'More details' screen
* UI layout optimized for both Phone and Tablets screens (not alot of UI to work with)
* Fetching and processing weather data for more locations
* Refresh the weather data periodically

**Started**
* Create Widget for Home Screen - It's there but it's not finished, also I would like to split a bit the concerns and treat it as a view in a MVP manner.

**Done**
* Ability to access application weather data from 3rd party apps via shared content provider
* Custom animation (Simple animation for when entering the first screen)
* All CRUD operations
* Use Kotlin instead of Java for MainActivity


### Libraries used

#### Android Support Library 
* **Appcompat** - Fragments, AppCompat, Material backwards compatibility 
* **Cardview** - For adding elevation (backwards compatibility)
* **RecyclerView** -  way better ListView, also comes with built-in animations
* **ConstraintLayout** - In order to avoid deep levels in UI hierarchy

#### Android Architecture Components
* **Lifecycle** - ViewModels and LiveData, also wanted to tie the Location via a dedicated Lifecycle observer to feed the location changes to the ViewModels.
* **Room** - Helps remove all the SQLite boilerplate

#### Play Services
* **Location** - Access to location services

#### Other
* **Dagger2** 
  * **Core** -  Easy to configure dependency injection framework
  * **Android** - allows for easier Inject into Activities, Fragments, Services, BroadcastReceiver, ContentProvider
  * **Android-support** - backwards compatibility
* **RxJava2** - Reactive extensions for Java.
  * **RxAndroid** - Android-specific Schedulers (mainThread or from a Looper)
* **Retrofit**  - Easy to use Networking Client
  * **rxAdapter** - Allow use of Observables for Retrofit calls
  * **gsonConverter** - Enables Retrofit to use Gson for JSON to POJO maping
  * **ohHttp-interceptor** - For logging networking calls
* **Stetho** - for debugging the Database 
* **Glide** - Image loading and caching
* **Reactive Location** - Wrapper arround Location Services which provides Observables instead of potential Callback Hell
* **Permission Dispatcher** - Neat library that removes permission handling boilerplate






