package com.weather.weatherbelike.domain;

import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.UIThread;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class GetLatestWeatherRequestTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    IWeatherRepo mWeatherRepo;

    @Mock
    ThreadExecutor mExecutor;

    @Mock
    UIThread mUiThread;

    private GetLatestWeatherRequest mLatestRequest;

    @Before
    public void setUp() {
        mLatestRequest = new GetLatestWeatherRequest(mExecutor, mUiThread, mWeatherRepo);
    }

    @Test
    public void shouldGetLatestWeatherRequest() {
        mLatestRequest.buildUseCaseObservable(null);
        verify(mWeatherRepo).getLatestWeatherRequest();
        verifyNoMoreInteractions(mWeatherRepo);
        verifyZeroInteractions(mExecutor);
        verifyZeroInteractions(mUiThread);
    }
}