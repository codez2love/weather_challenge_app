package com.weather.weatherbelike.domain;

import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.UIThread;
import com.weather.weatherbelike.data.cache.IAppCache;
import com.weather.weatherbelike.data.tools.ILocationProvider;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetNewWeatherInfoTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Mock
    IWeatherRepo mWeatherRepo;
    @Mock
    ThreadExecutor mExecutor;
    @Mock
    UIThread mUiThread;
    @Mock
    ILocationProvider mLocationProvider;
    @Mock
    IAppCache mAppCache;

    private GetNewWeatherInfo mGetNewWeatherInfo;

    @Before
    public void setUp() {
        mGetNewWeatherInfo = new GetNewWeatherInfo(mExecutor, mUiThread, mWeatherRepo, mLocationProvider, mAppCache);
    }

    @Test
    public void shouldGetNewWeatherInfo() {
        when(mAppCache.getDefaultCity()).thenReturn("Riga");

        mGetNewWeatherInfo.buildUseCaseObservable(null);
        verify(mWeatherRepo).getNewWeatherInfo("Riga");
        verifyNoMoreInteractions(mWeatherRepo);

        verifyZeroInteractions(mExecutor);
        verifyZeroInteractions(mUiThread);
    }
}