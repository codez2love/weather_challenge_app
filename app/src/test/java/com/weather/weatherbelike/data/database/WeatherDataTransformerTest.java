package com.weather.weatherbelike.data.database;

import com.weather.weatherbelike.data.api.TempInfo;
import com.weather.weatherbelike.data.api.WeatherDTO;
import com.weather.weatherbelike.data.api.WeatherInfo;
import com.weather.weatherbelike.data.utils.IDateFormatter;
import com.weather.weatherbelike.ui.model.WeatherDetailModel;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;

@RunWith(MockitoJUnitRunner.class)
public class WeatherDataTransformerTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    IDateFormatter mDateFormater;

    private WeatherDataTransformer mWeatherDataTransformer;

    @Before
    public void setUp() {
        mWeatherDataTransformer = new WeatherDataTransformer(mDateFormater);
    }

    @Test
    public void transformToEntity() {
        WeatherDTO dummyDto = buildDummyDTO();
        WeatherEntity result = mWeatherDataTransformer.transformToEntity(dummyDto);

        assertEquals(dummyDto.getTempInfo().getTemp(), result.getTemp(), 1);
        assertEquals(dummyDto.getTempInfo().getTemp_max(), result.getTempMax(), 1);
        assertEquals(dummyDto.getTempInfo().getTemp_min(), result.getTempMin(), 1);
        assertEquals(dummyDto.getTempInfo().getHumidity(), Double.parseDouble(result.getHumidity()), 1);
        assertEquals(dummyDto.getTempInfo().getPressure(), Double.parseDouble(result.getPressure()), 1);
        assertEquals(dummyDto.getCity(), result.getCity());
        assertEquals(System.currentTimeMillis(), result.getRequestStamp(), 1000);

    }

    @Test
    public void transformEntityToSimple() {
        String dummyDate = "22/10/2018";
        Mockito.when(mDateFormater.formatTimestamp(anyLong()))
                .thenReturn(dummyDate);
        WeatherEntity dummyEntity = WeatherEntity.dummy();

        WeatherSimpleModel result = mWeatherDataTransformer.tranformtoSimple(dummyEntity);

        assertEquals(dummyEntity.getCity(), result.getCity());
        assertEquals(dummyEntity.getDescription(), result.getDescription());
        assertEquals((long) dummyEntity.getId(), result.getId());
        assertEquals(dummyDate, result.getRequestTime());
        assertEquals(dummyEntity.getTemp(), result.getTemp(), 1);
    }

    @Test
    public void transformEntitiesListToSimpleModelList() {
        String dummyDate = "22/10/2018";
        Mockito.when(mDateFormater.formatTimestamp(anyLong()))
                .thenReturn(dummyDate);
        int count = 10;
        List<WeatherEntity> entities = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            entities.add(WeatherEntity.dummy());
        }
        List<WeatherSimpleModel> modelList
                = mWeatherDataTransformer.transformToSimpleModelList(entities);
        assertEquals(count, modelList.size());

        int commonIndex = 3;
        assertEquals(entities.get(commonIndex).getDescription(),
                modelList.get(commonIndex).getDescription());
    }

    @Test
    public void transformEntityToDetailModel() {
        String dummyDate = "22/10/2018";
        Mockito.when(mDateFormater.formatTimestamp(anyLong()))
                .thenReturn(dummyDate);
        WeatherEntity dummyEntity = WeatherEntity.dummy();
        WeatherDetailModel result = mWeatherDataTransformer.transformToDetailModel(dummyEntity);

        assertEquals(dummyDate, result.getRequestTime());
        assertEquals(dummyEntity.getCity(), result.getCity());
    }

/*
    @Test
    public void transformContentValuesToEntity() {
        long dummyId = 10;
        double dummyTemp = 150.0;
        long dummyTimestamp = System.currentTimeMillis();
        ContentValues cvs = new ContentValues();
        cvs.put(WeatherEntity.COLUMN_ID,dummyId);
        cvs.put(WeatherEntity.COLUMN_TEMP,dummyTemp);
        cvs.put(WeatherEntity.COLUMN_REQUEST_STAMP,dummyTimestamp);

        WeatherEntity result = mWeatherDataTransformer.transformIntoEntity(cvs);

        assertEquals(dummyId,(long) result.getId());
        assertEquals(dummyTemp,result.getTemp(),1);
        assertEquals(dummyTimestamp,(long) result.getRequestStamp());
    }
*/

    public static WeatherDTO buildDummyDTO() {
        List<WeatherInfo> weatherInfoList = new ArrayList<>();
        weatherInfoList.add(new WeatherInfo(10, "main", "desc", "sunny"));
        TempInfo tempInfo = new TempInfo(250.0, 20.0, 25.0, 245.4, 267.4);
        WeatherDTO dto = new WeatherDTO(10, weatherInfoList, "base", tempInfo, "Riga");
        return dto;
    }

}