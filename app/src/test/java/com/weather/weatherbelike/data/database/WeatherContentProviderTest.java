package com.weather.weatherbelike.data.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;

import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowContentResolver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

//@RunWith(RobolectricTestRunner.class)
//@Config(manifest = Config.NONE)
public class WeatherContentProviderTest {

    private ContentResolver mResolver;

    //TODO Not finished
    //@Test
    public void setUp() {
        mResolver = RuntimeEnvironment.application.getContentResolver();
        WeatherContentProvider cp = new WeatherContentProvider();
        cp.onCreate();
        ShadowContentResolver.registerProviderInternal(WeatherContentProvider.AUTHORITY, cp);
    }

    //@Test
    public void query() {
        Cursor cursor = mResolver.query(WeatherContentProvider.WEATHER_CONTENT_URI, null, null, null, null);
        assertNull(cursor);

        ContentValues cvs = new ContentValues();
        cvs.put(WeatherEntity.COLUMN_CITY, "TestCity");
        cvs.put(WeatherEntity.COLUMN_DESCRIPTION, "SomeDesc");
        mResolver.insert(WeatherContentProvider.WEATHER_CONTENT_URI, cvs);

        cursor = mResolver.query(WeatherContentProvider.WEATHER_CONTENT_URI, null, null, null, null);
        assertEquals(1, cursor.getCount());


    }

}