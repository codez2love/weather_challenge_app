package com.weather.weatherbelike.data.cache;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;


@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class MainCacheTest {

    private MainCache mCache;

    @Before
    public void setUp() {
        mCache = new MainCache(RuntimeEnvironment.application);
    }

    @Test
    public void setDefaultCity() {
        String testString = "testCity";
        mCache.setDefaultCity(testString);
        assertEquals(mCache.getDefaultCity(), testString);
    }

    @Test
    public void getDefaultCity_ifSPEmptyShouldDefault() {
        String testString = "default";
        assertEquals(testString, mCache.getDefaultCity());
    }

    @Test
    public void setTemperaturePreference() {
        int testPreference = IAppCache.PREF_CELSIUS;

        mCache.setTemperaturePreference(testPreference);
        assertEquals(testPreference, mCache.getTemperaturePreference());


        testPreference = IAppCache.PREF_FAHRENHEIT;
        mCache.setTemperaturePreference(testPreference);
        assertEquals(testPreference, mCache.getTemperaturePreference());
    }
}