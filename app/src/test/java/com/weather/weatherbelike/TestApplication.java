package com.weather.weatherbelike;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class TestApplication extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("========== Initializing TestApplication ==========");
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return null;
    }
}
