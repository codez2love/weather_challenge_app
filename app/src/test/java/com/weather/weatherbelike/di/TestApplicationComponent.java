package com.weather.weatherbelike.di;

import com.weather.weatherbelike.TestApplication;

import dagger.Component;
import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;

@Component(
        modules = {
                AndroidInjection.class,
                AndroidSupportInjection.class,
                TestAppModule.class,
                TestAppBindings.class,
        }
)
public interface TestApplicationComponent {
    void inject(TestApplication app);
}
