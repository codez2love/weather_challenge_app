package com.weather.weatherbelike.di;

import com.weather.weatherbelike.data.database.WeatherContentProvider;
import com.weather.weatherbelike.ui.MainActivity;
import com.weather.weatherbelike.ui.WeatherListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TestAppBindings {

    @ContributesAndroidInjector
    abstract WeatherContentProvider weatherContentProvider();

    @ContributesAndroidInjector(modules = AppBindings.MainActivityModule.class)
    abstract MainActivity mainActivity();

    @Module
    static abstract class MainActivityModule {
        @ContributesAndroidInjector
        abstract WeatherListFragment weatherListFragment();
    }

}
