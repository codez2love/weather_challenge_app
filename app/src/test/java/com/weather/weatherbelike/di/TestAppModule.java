package com.weather.weatherbelike.di;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.res.Resources;

import com.weather.weatherbelike.JobExecutor;
import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.cache.IAppCache;
import com.weather.weatherbelike.data.cache.MainCache;
import com.weather.weatherbelike.data.repository.WeatherRepository;
import com.weather.weatherbelike.data.repository.local.IWeatherLocalStore;
import com.weather.weatherbelike.data.repository.local.IWeatherRoomStore;
import com.weather.weatherbelike.data.repository.local.WeatherLocalStore;
import com.weather.weatherbelike.data.tools.ILocationProvider;
import com.weather.weatherbelike.domain.IWeatherRepo;

import org.mockito.Mockito;
import org.robolectric.RuntimeEnvironment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(subcomponents = ViewModelFactory.ViewModelSubComponent.class)
public class TestAppModule {

    public TestAppModule() {
    }

    @Provides
    Context provideAppContext() {
        return RuntimeEnvironment.application;
    }

    @Provides
    Resources provideAndroidResources(Context context) {
        return context.getResources();
    }

    @Provides
    ILocationProvider provideLocationProvider() {
        ILocationProvider locationProvider = Mockito.mock(ILocationProvider.class);
        Mockito.when(locationProvider.getLastKnownLocation())
                .thenReturn(null);
        return locationProvider;
    }

    @Provides
    static ThreadExecutor provideThreadExecutor(JobExecutor executor) {
        return executor;
    }

    @Provides
    static IWeatherRepo provideWeatherRepo(WeatherRepository repository) {
        return repository;
    }

    @Provides
    static IWeatherLocalStore provideWeatherLocalStore(WeatherLocalStore localStore) {
        return localStore;
    }

    @Provides
    static IWeatherRoomStore provideRoomStore(WeatherLocalStore localStore) {
        return localStore;
    }

    @Provides
    static IAppCache provideAppCache(MainCache cache) {
        return cache;
    }

    @Singleton
    @Provides
    static ViewModelProvider.Factory provideViewModelFactory(
            ViewModelFactory.ViewModelSubComponent.Builder viewModelSubComponent) {
        return new ViewModelFactory(viewModelSubComponent.build());
    }

}
