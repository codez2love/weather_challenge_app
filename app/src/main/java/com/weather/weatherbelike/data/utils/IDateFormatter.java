package com.weather.weatherbelike.data.utils;

public interface IDateFormatter {
    String formatTimestamp(long timeStamp);
}
