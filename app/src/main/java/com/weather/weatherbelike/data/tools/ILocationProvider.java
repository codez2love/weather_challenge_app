package com.weather.weatherbelike.data.tools;

import android.location.Location;

import io.reactivex.Single;

public interface ILocationProvider {
    Single<Location> getLastKnownLocation();
}
