package com.weather.weatherbelike.data.cache;

public interface IAppCache {

    static final int PREF_FAHRENHEIT = 1;
    static final int PREF_CELSIUS = 2;

    void setDefaultCity(String city);

    String getDefaultCity();

    void setTemperaturePreference(int prefType);

    int getTemperaturePreference();
}
