package com.weather.weatherbelike.data.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.weather.weatherbelike.BuildConfig;
import com.weather.weatherbelike.data.repository.local.IWeatherRoomStore;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class WeatherContentProvider extends ContentProvider {
    private static final String TAG = "WeatherContentProv";

    public static final String SELECT_NEWEST = "newest";

    public static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".weather_provider";
    static final String MAIN_URI = "content://" + AUTHORITY + "/";

    public static final Uri WEATHER_CONTENT_URI = Uri.parse(MAIN_URI + WeatherEntity.TABLE_NAME);

    private static final int CODE_WEATHER_DIR = 1;
    private static final int CODE_WEATHER_ITEM = 2;

    private static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        MATCHER.addURI(AUTHORITY, WeatherEntity.TABLE_NAME, CODE_WEATHER_DIR);
        MATCHER.addURI(AUTHORITY, WeatherEntity.TABLE_NAME + "/#", CODE_WEATHER_ITEM);
    }

    @Inject
    IWeatherRoomStore mRoomStore;

    @Inject
    WeatherDataTransformer mWeatherDataTransformer;

    @Override
    public boolean onCreate() {
        AndroidInjection.inject(this);
        return true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (MATCHER.match(uri)) {
            case CODE_WEATHER_DIR:
                return "vnd.android.cursor.dir/" + AUTHORITY + "." + WeatherEntity.TABLE_NAME;
            case CODE_WEATHER_ITEM:
                return "vnd.android.cursor.item/" + AUTHORITY + "." + WeatherEntity.TABLE_NAME;
            default:
                throw new IllegalArgumentException("Unknown URI:" + uri);
        }
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        verifyContext(getContext());
        int code = MATCHER.match(uri);
        if (code == CODE_WEATHER_ITEM || code == CODE_WEATHER_DIR) {
            Cursor cursor;
            if (code == CODE_WEATHER_DIR) {
                if (selection != null && selection.equals(SELECT_NEWEST)) {
                    cursor = mRoomStore.selectNewest();
                } else {
                    cursor = mRoomStore.selectAll();
                }
            } else {
                cursor = mRoomStore.selectById(ContentUris.parseId(uri));
            }
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
            return cursor;
        }
        return null;
    }


    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        verifyContext(getContext());
        switch (MATCHER.match(uri)) {
            case CODE_WEATHER_DIR:
                long id = mRoomStore.insert(mWeatherDataTransformer.transformIntoEntity(values));
                getContext().getContentResolver().notifyChange(uri, null);
                return ContentUris.withAppendedId(uri, id);
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        verifyContext(getContext());
        switch (MATCHER.match(uri)) {
            case CODE_WEATHER_ITEM:
                int count = mRoomStore.deleteById(ContentUris.parseId(uri));
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        verifyContext(getContext());
        switch (MATCHER.match(uri)) {
            case CODE_WEATHER_ITEM:
                int count = mRoomStore.update(mWeatherDataTransformer.transformIntoEntity(values));
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    static void verifyContext(Context context) {
        if (context == null) {
            Log.e(TAG, "Provider failed due to missing context");
            throw new RuntimeException("Context is null");
        }
    }
}
