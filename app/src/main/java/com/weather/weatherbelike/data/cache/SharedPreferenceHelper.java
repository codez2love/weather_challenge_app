package com.weather.weatherbelike.data.cache;

import android.content.SharedPreferences;

public abstract class SharedPreferenceHelper {

    private String mStringDefault = "";
    private int mIntDefault = 0;
    private boolean mBooleanDefault = false;

    public abstract SharedPreferences getSP();

    protected void putString(String key, String value) {
        getSP()
                .edit()
                .putString(key, value)
                .apply();
    }

    protected void putInt(String key, int value) {
        getSP()
                .edit()
                .putInt(key, value)
                .apply();
    }

    protected void putBoolean(String key, boolean value) {
        getSP()
                .edit()
                .putBoolean(key, value)
                .apply();
    }

    protected void setStringDefaultValue(String defaults) {
        mStringDefault = defaults;
    }

    protected void setIntDefaultValue(int defaults) {
        mIntDefault = defaults;
    }

    protected void setBooleanDefaultValue(boolean defaultValue) {
        mBooleanDefault = defaultValue;
    }

    protected String getStringFromPreferences(String key, String defaults) {
        return getSP().getString(key, defaults);
    }

    protected String getStringFromPreferences(String key) {
        return getSP().getString(key, mStringDefault);
    }

    protected int getIntFromPreferences(String key, int defaults) {
        return getSP().getInt(key, defaults);
    }

    protected int getIntFromPreferences(String key) {
        return getSP().getInt(key, mIntDefault);
    }

    protected boolean getBooleanFromPreferences(String key, boolean defaults) {
        return getSP().getBoolean(key, defaults);
    }

    protected boolean getBooleanFromPreferences(String key) {
        return getSP().getBoolean(key, mBooleanDefault);
    }
}

