package com.weather.weatherbelike.data.repository;

import com.weather.weatherbelike.data.api.WeatherDTO;
import com.weather.weatherbelike.data.database.WeatherDataTransformer;
import com.weather.weatherbelike.data.database.WeatherEntity;
import com.weather.weatherbelike.data.repository.local.IWeatherLocalStore;
import com.weather.weatherbelike.data.repository.remote.WeatherRemoteStore;
import com.weather.weatherbelike.domain.IWeatherRepo;
import com.weather.weatherbelike.ui.model.WeatherDetailModel;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Singleton
public class WeatherRepository implements IWeatherRepo {

    private IWeatherLocalStore mLocalSource;
    private WeatherDataTransformer mWeatherDataTransformer;
    private WeatherRemoteStore mRemoteSource;

    @Inject
    public WeatherRepository(IWeatherLocalStore localDataSource,
                             WeatherRemoteStore remoteDataSource,
                             WeatherDataTransformer weatherDataTransformer) {
        mLocalSource = localDataSource;
        mRemoteSource = remoteDataSource;
        this.mWeatherDataTransformer = weatherDataTransformer;
    }

    @Override
    public Single<WeatherSimpleModel> getNewWeatherInfo(final String city) {
        return mRemoteSource.getNewWeatherInfo(city)
                .map(this::saveAndTransform);
    }

    @Override
    public Single<WeatherSimpleModel> getNewWeatherInfo(double lat, double lon) {
        return mRemoteSource.getNewWeatherInfo(lat, lon)
                .map(this::saveAndTransform);
    }

    @Override
    public Single<WeatherDetailModel> getWeatherItemDetails(Long id) {
        return mLocalSource.getWeatherItemDetails(id)
                .map(mWeatherDataTransformer::transformToDetailModel);
    }

    @Override
    public Flowable<List<WeatherSimpleModel>> getWeatherRequestHistory() {
        return mLocalSource.getWeatherRequestHistory()
                .map(mWeatherDataTransformer::transformToSimpleModelList);
    }

    @Override
    public Single<WeatherDetailModel> getLatestWeatherRequest() {
        return mLocalSource.getLatestWeatherRequest().map(mWeatherDataTransformer::transformToDetailModel);
    }

    private WeatherSimpleModel saveAndTransform(WeatherDTO weatherDTO) {
        WeatherEntity entity = mWeatherDataTransformer.transformToEntity(weatherDTO);
        long id = mLocalSource.addNewWeatherInfo(entity);
        entity.setId(id);
        return mWeatherDataTransformer.tranformtoSimple(entity);
    }

}
