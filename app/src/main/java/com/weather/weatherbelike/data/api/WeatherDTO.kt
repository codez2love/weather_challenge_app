package com.weather.weatherbelike.data.api

import com.google.gson.annotations.SerializedName

data class WeatherDTO(
        val id: Int,
        @SerializedName("weather")
        val weatherInfo: List<WeatherInfo>,
        val base: String,
        @SerializedName("main")
        val tempInfo: TempInfo,
        @SerializedName("name")
        val city: String)


data class WeatherInfo(
        val id: Int,
        val main: String,
        val description: String,
        val icon: String)

data class TempInfo(
        val temp: Double,
        val pressure: Double,
        val humidity: Double,
        val temp_min: Double,
        val temp_max: Double)


