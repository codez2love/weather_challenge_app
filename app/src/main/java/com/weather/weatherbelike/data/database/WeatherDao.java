package com.weather.weatherbelike.data.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface WeatherDao {

    @Insert
    Long insertNewWeatherDetails(WeatherEntity newEntity);

    @Query("SELECT * FROM weather_info ORDER BY request_stamp")
    Cursor selectAll();

    @Query("SELECT * from weather_info ORDER BY request_stamp DESC")
    Flowable<List<WeatherEntity>> getAllWeatherHistory();

    @Query("SELECT * FROM weather_info WHERE id = :id")
    Cursor selectById(long id);

    @Query("SELECT * from weather_info WHERE id = :id")
    Single<WeatherEntity> getWeatherItemDetails(Long id);

    @Query("SELECT * from weather_info ORDER BY request_stamp DESC LIMIT 1")
    Cursor selectNewest();

    @Query("SELECT * from weather_info ORDER BY request_stamp DESC LIMIT 1")
    Single<WeatherEntity> getLatestWeatherRequest();

    @Query("DELETE FROM weather_info WHERE id = :id")
    int deleteById(long id);

    @Update
    int update(WeatherEntity entity);
}
