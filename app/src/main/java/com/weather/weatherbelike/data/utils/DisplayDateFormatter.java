package com.weather.weatherbelike.data.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

public class DisplayDateFormatter implements IDateFormatter {
    private final SimpleDateFormat mDisplayDateFormat
            = new SimpleDateFormat("dd/MM HH:mm", Locale.ENGLISH);

    @Inject
    public DisplayDateFormatter() {
    }

    @Override
    public String formatTimestamp(long timestamp) {
        Date date = new Date(timestamp);
        return mDisplayDateFormat.format(date);
    }

}
