package com.weather.weatherbelike.data.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

@Database(entities = {WeatherEntity.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {
    public abstract WeatherDao weatherDao();

    public static Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE " + WeatherEntity.TABLE_NAME + " ADD COLUMN " + WeatherEntity.COLUMN_CITY);
        }
    };

    public static Migration MIGRATION_1_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE " + WeatherEntity.TABLE_NAME + " ADD COLUMN " + WeatherEntity.COLUMN_ICON_URL);
        }
    };
}
