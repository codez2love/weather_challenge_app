package com.weather.weatherbelike.data.tools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;

import io.reactivex.Single;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;

public class LocationProvider implements ILocationProvider {

    private ReactiveLocationProvider mLocationProvider;

    public LocationProvider(Context context) {
        mLocationProvider = new ReactiveLocationProvider(context);
    }

    @SuppressLint("MissingPermission")
    @Override
    public Single<Location> getLastKnownLocation() {
        return mLocationProvider.getLastKnownLocation()
                .doOnError(Throwable::printStackTrace)
                .firstOrError();
    }
/*
    @SuppressLint("MissingPermission")
    public static void getLastKnownLocation(Activity activity, final OnSuccessListener<Location> onSuccessListener) {
        FusedLocationProviderClient locationClient = LocationServices.getFusedLocationProviderClient(activity);
        locationClient.getLastLocation()
                .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if(location!=null){
                            onSuccessListener.onSuccess(location);
                        } else {
                            Log.e("LocationProvider","Failed to retreive last location");
                        }
                    }
                })
                .addOnFailureListener(activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });
    }*/
}
