package com.weather.weatherbelike.data;

import java.util.concurrent.Executor;

public interface ThreadExecutor extends Executor {
}
