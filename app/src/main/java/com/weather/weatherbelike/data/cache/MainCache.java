package com.weather.weatherbelike.data.cache;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class MainCache extends SharedPreferenceHelper implements IAppCache {

    private static final String CACHE_NAME = "main_cache";
    private static final String DEFAULT_CITY = "default_city";
    private static final String PREF_TEMP_TYPE = "temp_type";

    private Context mContext;

    @Inject
    public MainCache(Context context) {
        mContext = context;
    }

    @Override
    public void setDefaultCity(String city) {
        putString(DEFAULT_CITY, city);
    }

    @Override
    public String getDefaultCity() {
        return getStringFromPreferences(DEFAULT_CITY, "default");
    }

    @Override
    public void setTemperaturePreference(int prefType) {
        putInt(PREF_TEMP_TYPE, prefType);
    }

    @Override
    public int getTemperaturePreference() {
        return getIntFromPreferences(PREF_TEMP_TYPE, IAppCache.PREF_CELSIUS);
    }

    @Override
    public SharedPreferences getSP() {
        return mContext.getSharedPreferences(CACHE_NAME, Context.MODE_PRIVATE);
    }
}
