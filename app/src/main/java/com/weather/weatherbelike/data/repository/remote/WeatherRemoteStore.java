package com.weather.weatherbelike.data.repository.remote;

import com.weather.weatherbelike.data.api.WeatherApis;
import com.weather.weatherbelike.data.api.WeatherDTO;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Single;

public class WeatherRemoteStore {

    private WeatherApis mWeatherApis;
    private String mApiKey;

    @Inject
    public WeatherRemoteStore(WeatherApis weatherApis,
                              @Named("APPID") String apikey) {
        mWeatherApis = weatherApis;
        mApiKey = apikey;
    }

    public Single<WeatherDTO> getNewWeatherInfo(String city) {
        return mWeatherApis.getWeather(city, mApiKey);
    }

    public Single<WeatherDTO> getNewWeatherInfo(double lat, double lon) {
        return mWeatherApis.getWeatherByCoords(lat, lon, mApiKey);
    }
}
