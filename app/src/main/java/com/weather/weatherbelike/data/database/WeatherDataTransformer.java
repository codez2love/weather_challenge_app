package com.weather.weatherbelike.data.database;

import android.content.ContentValues;
import android.database.Cursor;

import com.weather.weatherbelike.data.api.WeatherDTO;
import com.weather.weatherbelike.data.utils.IDateFormatter;
import com.weather.weatherbelike.di.WeatherNetModule;
import com.weather.weatherbelike.ui.model.WeatherDetailModel;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import java.util.List;
import java.util.Vector;

import javax.inject.Inject;

public class WeatherDataTransformer {

    IDateFormatter mDateFormater;

    @Inject
    public WeatherDataTransformer(IDateFormatter dateFormatter) {
        mDateFormater = dateFormatter;
    }

    public WeatherEntity transformToEntity(WeatherDTO weatherDto) {
        WeatherEntity entityJ = new WeatherEntity();
        if (weatherDto.getWeatherInfo().size() > 0) {
            entityJ.setDescription(weatherDto.getWeatherInfo().get(0).getDescription() + "");
            entityJ.setIconId(weatherDto.getWeatherInfo().get(0).getIcon());
        }
        entityJ.setTemp(weatherDto.getTempInfo().getTemp());
        entityJ.setHumidity(weatherDto.getTempInfo().getHumidity() + "");
        entityJ.setPressure(weatherDto.getTempInfo().getPressure() + "");
        entityJ.setTempMin(weatherDto.getTempInfo().getTemp_min());
        entityJ.setTempMax(weatherDto.getTempInfo().getTemp_max());
        entityJ.setCity(weatherDto.getCity());
        entityJ.setRequestStamp(System.currentTimeMillis());
        return entityJ;
    }

    public WeatherSimpleModel tranformtoSimple(WeatherEntity entity) {
        String iconUrl = buildIconUrl(entity.getIconId());
        String requestTime = mDateFormater.formatTimestamp(entity.getRequestStamp());
        return new WeatherSimpleModel(
                entity.getId() == null ? 0 : entity.getId(),
                entity.getTemp(),
                entity.getCity(),
                requestTime,
                entity.getDescription(),
                iconUrl);
    }

    public List<WeatherSimpleModel> transformToSimpleModelList(List<WeatherEntity> entities) {
        List<WeatherSimpleModel> temp = new Vector<>();
        for (int i = 0; i < entities.size(); i++) {
            temp.add(tranformtoSimple(entities.get(i)));
        }
        return temp;
    }

    public WeatherDetailModel transformToDetailModel(WeatherEntity entity) {
        String reqTimeFormatted = mDateFormater.formatTimestamp(entity.getRequestStamp());
        String iconUrl = buildIconUrl(entity.getIconId());
        return new WeatherDetailModel(entity.getTempMin(), entity.getTempMax(), entity.getTemp(), entity.getDescription(),
                entity.getPressure(), entity.getHumidity(), reqTimeFormatted, entity.getCity(), iconUrl);
    }

    public WeatherEntity transformIntoEntity(Cursor cursor) {
        if (cursor == null || !cursor.moveToFirst()) {
            return WeatherEntity.dummy();
        }
        WeatherEntity entity = new WeatherEntity();
        entity.setId(cursor.getLong(cursor.getColumnIndex(WeatherEntity.COLUMN_ID)));
        entity.setDescription(cursor.getString(cursor.getColumnIndex(WeatherEntity.COLUMN_DESCRIPTION)));
        entity.setTemp(cursor.getDouble(cursor.getColumnIndex(WeatherEntity.COLUMN_TEMP)));
        entity.setTempMin(cursor.getDouble(cursor.getColumnIndex(WeatherEntity.COLUMN_TEMP_MAX)));
        entity.setTempMax(cursor.getDouble(cursor.getColumnIndex(WeatherEntity.COLUMN_TEMP_MIN)));
        entity.setPressure(cursor.getString(cursor.getColumnIndex(WeatherEntity.COLUMN_PRESSURE)));
        entity.setHumidity(cursor.getString(cursor.getColumnIndex(WeatherEntity.COLUMN_HUMIDITY)));
        entity.setCity(cursor.getString(cursor.getColumnIndex(WeatherEntity.COLUMN_CITY)));
        entity.setRequestStamp(cursor.getLong(cursor.getColumnIndex(WeatherEntity.COLUMN_REQUEST_STAMP)));
        return entity;
    }

    public WeatherEntity transformIntoEntity(ContentValues cvs) {
        WeatherEntity entityJ = new WeatherEntity();
        if (cvs.containsKey(WeatherEntity.COLUMN_ID)) {
            entityJ.setId(cvs.getAsLong(WeatherEntity.COLUMN_ID));
        }
        if (cvs.containsKey(WeatherEntity.COLUMN_TEMP)) {
            entityJ.setTemp(cvs.getAsDouble(WeatherEntity.COLUMN_TEMP));
        }
        if (cvs.containsKey(WeatherEntity.COLUMN_DESCRIPTION)) {
            entityJ.setDescription(cvs.getAsString(WeatherEntity.COLUMN_DESCRIPTION));
        }
        if (cvs.containsKey(WeatherEntity.COLUMN_TEMP_MIN)) {
            entityJ.setTempMin(cvs.getAsDouble(WeatherEntity.COLUMN_TEMP_MIN));
        }
        if (cvs.containsKey(WeatherEntity.COLUMN_TEMP_MAX)) {
            entityJ.setTempMax(cvs.getAsDouble(WeatherEntity.COLUMN_TEMP_MAX));
        }
        if (cvs.containsKey(WeatherEntity.COLUMN_PRESSURE)) {
            entityJ.setPressure(cvs.getAsString(WeatherEntity.COLUMN_PRESSURE));
        }
        if (cvs.containsKey(WeatherEntity.COLUMN_HUMIDITY)) {
            entityJ.setHumidity(cvs.getAsString(WeatherEntity.COLUMN_HUMIDITY));
        }
        if (cvs.containsKey(WeatherEntity.COLUMN_REQUEST_STAMP)) {
            entityJ.setRequestStamp(cvs.getAsLong(WeatherEntity.COLUMN_REQUEST_STAMP));
        }
        return entityJ;
    }

    public String buildIconUrl(String iconId) {
        return WeatherNetModule.IMG_URL + iconId + ".png";
    }
}
