package com.weather.weatherbelike.data.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import static com.weather.weatherbelike.data.database.WeatherEntity.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class WeatherEntity {
    public static final String TABLE_NAME = "weather_info";

    protected static final String COLUMN_ID = "id";
    protected static final String COLUMN_TEMP = "temp";
    protected static final String COLUMN_DESCRIPTION = "description";
    protected static final String COLUMN_TEMP_MIN = "temp_min";
    protected static final String COLUMN_TEMP_MAX = "temp_max";
    protected static final String COLUMN_PRESSURE = "pressure";
    protected static final String COLUMN_HUMIDITY = "humidity";
    protected static final String COLUMN_REQUEST_STAMP = "request_stamp";
    protected static final String COLUMN_CITY = "city";
    protected static final String COLUMN_ICON_URL = "icon_url";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COLUMN_ID)
    private Long mId;
    @ColumnInfo(name = COLUMN_TEMP)
    private Double mTemp;
    @ColumnInfo(name = COLUMN_DESCRIPTION)
    private String mDescription;
    @ColumnInfo(name = COLUMN_TEMP_MIN)
    private Double mTempMin;
    @ColumnInfo(name = COLUMN_TEMP_MAX)
    private Double mTempMax;
    @ColumnInfo(name = COLUMN_PRESSURE)
    private String mPressure;
    @ColumnInfo(name = COLUMN_HUMIDITY)
    private String mHumidity;
    @ColumnInfo(name = COLUMN_REQUEST_STAMP)
    private Long mRequestStamp;
    @ColumnInfo(name = COLUMN_CITY)
    private String mCity;
    @ColumnInfo(name = COLUMN_ICON_URL)
    private String mIconId;

    public WeatherEntity() {
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Double getTemp() {
        return mTemp;
    }

    public void setTemp(Double temp) {
        mTemp = temp;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Double getTempMin() {
        return mTempMin;
    }

    public void setTempMin(Double tempMin) {
        mTempMin = tempMin;
    }

    public Double getTempMax() {
        return mTempMax;
    }

    public void setTempMax(Double tempMax) {
        mTempMax = tempMax;
    }

    public String getPressure() {
        return mPressure;
    }

    public void setPressure(String pressure) {
        mPressure = pressure;
    }

    public String getHumidity() {
        return mHumidity;
    }

    public void setHumidity(String humidity) {
        mHumidity = humidity;
    }

    public void setRequestStamp(Long timestamp) {
        mRequestStamp = timestamp;
    }

    public Long getRequestStamp() {
        return mRequestStamp;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String mCity) {
        this.mCity = mCity;
    }

    public String getIconId() {
        return mIconId;
    }

    public void setIconId(String mIcon) {
        this.mIconId = mIcon;
    }

    public static WeatherEntity dummy() {
        WeatherEntity dummy = new WeatherEntity();
        dummy.setId(0L);
        dummy.setCity("City");
        dummy.setRequestStamp(19200L);
        dummy.setHumidity("Humidity");
        dummy.setPressure("Pressure");
        dummy.setTempMax(0.0);
        dummy.setTempMin(0.0);
        dummy.setTemp(0.0);
        dummy.setDescription("Description");
        return dummy;
    }
}
