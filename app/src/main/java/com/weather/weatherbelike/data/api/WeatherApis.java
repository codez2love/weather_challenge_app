package com.weather.weatherbelike.data.api;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApis {

    @GET("data/2.5/weather")
    Single<WeatherDTO> getWeather(@Query("q") String cityName,
                                  @Query("APPID") String appId);

    @GET("data/2.5/weather")
    Single<WeatherDTO> getWeatherByCoords(@Query("lat") double lat,
                                          @Query("lon") double lon,
                                          @Query("APPID") String appId);

}
