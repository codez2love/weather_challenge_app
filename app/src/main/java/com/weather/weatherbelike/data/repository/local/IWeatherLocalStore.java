package com.weather.weatherbelike.data.repository.local;

import com.weather.weatherbelike.data.database.WeatherEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface IWeatherLocalStore {
    long addNewWeatherInfo(WeatherEntity entity);

    Single<WeatherEntity> getWeatherItemDetails(Long id);

    Flowable<List<WeatherEntity>> getWeatherRequestHistory();

    Single<WeatherEntity> getLatestWeatherRequest();
}
