package com.weather.weatherbelike.data.repository.local;

import android.database.Cursor;

import com.weather.weatherbelike.data.database.WeatherDao;
import com.weather.weatherbelike.data.database.WeatherEntity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class WeatherLocalStore implements IWeatherLocalStore, IWeatherRoomStore {

    private WeatherDao mWeatherDao;

    @Inject
    public WeatherLocalStore(WeatherDao weatherDao) {
        mWeatherDao = weatherDao;
    }

    @Override
    public long addNewWeatherInfo(WeatherEntity entity) {
        return mWeatherDao.insertNewWeatherDetails(entity);
    }

    @Override
    public Flowable<List<WeatherEntity>> getWeatherRequestHistory() {
        return mWeatherDao.getAllWeatherHistory().take(1);
    }

    @Override
    public Single<WeatherEntity> getWeatherItemDetails(Long id) {
        return mWeatherDao.getWeatherItemDetails(id);
    }

    @Override
    public Cursor selectAll() {
        return mWeatherDao.selectAll();
    }

    @Override
    public Cursor selectNewest() {
        return mWeatherDao.selectNewest();
    }

    @Override
    public Cursor selectById(long id) {
        return mWeatherDao.selectById(id);
    }

    @Override
    public long insert(WeatherEntity entityJ) {
        return mWeatherDao.insertNewWeatherDetails(entityJ);
    }

    @Override
    public int update(WeatherEntity entityJ) {
        return mWeatherDao.update(entityJ);
    }

    @Override
    public int deleteById(long id) {
        return mWeatherDao.deleteById(id);
    }

    @Override
    public Single<WeatherEntity> getLatestWeatherRequest() {
        return mWeatherDao.getLatestWeatherRequest();
    }
}
