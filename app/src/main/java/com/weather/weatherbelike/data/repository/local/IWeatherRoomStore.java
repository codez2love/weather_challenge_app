package com.weather.weatherbelike.data.repository.local;

import android.database.Cursor;

import com.weather.weatherbelike.data.database.WeatherEntity;

public interface IWeatherRoomStore {

    Cursor selectAll();

    Cursor selectNewest();

    Cursor selectById(long id);

    long insert(WeatherEntity entityJ);

    int update(WeatherEntity entityJ);

    int deleteById(long id);

}
