package com.weather.weatherbelike.data.utils;

public class OtherUtils {

    public static int convertToCelsius(double kelvin) {
        return (int) (kelvin - 273.16);
    }

    public static double convertToFahrenheit(double kelvin) {
        return convertToCelsius(kelvin) * 9 / 5 + 32;
    }
}
