package com.weather.weatherbelike.ui.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.RemoteViews;

import com.weather.weatherbelike.R;
import com.weather.weatherbelike.data.database.WeatherContentProvider;
import com.weather.weatherbelike.data.database.WeatherDataTransformer;
import com.weather.weatherbelike.domain.GetLatestWeatherRequest;
import com.weather.weatherbelike.ui.MainActivity;
import com.weather.weatherbelike.ui.model.WeatherDetailModel;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Implementation of App Widget functionality.
 */
public class WeatherWidgetProvider extends AppWidgetProvider {
    private static final String TAG = "WeatherWidget";

    private int FAILED_COUNTER = 0;

    @Inject
    GetLatestWeatherRequest mGetLatestWeatherRequest;

    @Inject
    WeatherDataTransformer mWeatherDataTransformer;

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        checkInjection(context);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        checkInjection(context);
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        checkInjection(context);
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        checkInjection(context);
        // Enter relevant functionality for when the last widget is disabled
    }

    private void checkInjection(Context context) {
        if (mGetLatestWeatherRequest == null) {
            AndroidInjection.inject(this, context);
            if (mGetLatestWeatherRequest == null) {
                Log.e(TAG, "Failed to perform injection in widget: " + ++FAILED_COUNTER);
            }
        }
    }

    void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                         int appWidgetId) {
        // Construct the RemoteViews object
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.weather_widget);
        views.setOnClickPendingIntent(R.id.widget_view, pendingIntent);

        fetchNewDataUc(views, appWidgetManager, appWidgetId);
    }

    private void fetchNewDataUc(final RemoteViews remoteViews,
                                final AppWidgetManager appWidgetManager,
                                final int id) {
        mGetLatestWeatherRequest.execute(new DisposableObserver<WeatherDetailModel>() {
            @Override
            public void onNext(WeatherDetailModel weatherDetailModel) {
                setData(weatherDetailModel, remoteViews, appWidgetManager, id);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "Failed to getLatestWeatherRequest", e);
            }

            @Override
            public void onComplete() {
                Log.i(TAG, "Fetched latest data");
            }
        }, null);
    }

    private void fetchNewData(final Context context,
                              final RemoteViews remoteViews,
                              final AppWidgetManager appWidgetManager,
                              final int id) {
        Disposable dis = Single.just(0)
                .map(integer -> {
                    Cursor cursor = context.getContentResolver().query(WeatherContentProvider.WEATHER_CONTENT_URI, null, WeatherContentProvider.SELECT_NEWEST, null, null);
                    return mWeatherDataTransformer.transformToDetailModel(mWeatherDataTransformer.transformIntoEntity(cursor));
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(model -> setData(model, remoteViews, appWidgetManager, id),
                        Throwable::printStackTrace);
    }

    private void setData(WeatherDetailModel model, RemoteViews views, AppWidgetManager appWidgetManager, int appWidgetId) {
        views.setTextViewText(R.id.tv_temp, model.getMaxTemp() + "");
        views.setTextViewText(R.id.tv_city, model.getCity());
        views.setTextViewText(R.id.tv_last_updated, model.getRequestTime());

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
}



