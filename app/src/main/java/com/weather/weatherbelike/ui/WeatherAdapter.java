package com.weather.weatherbelike.ui;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.weather.weatherbelike.R;
import com.weather.weatherbelike.data.cache.IAppCache;
import com.weather.weatherbelike.data.utils.OtherUtils;
import com.weather.weatherbelike.di.GlideApp;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import java.util.List;
import java.util.Vector;

import javax.inject.Inject;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherDefaultViewHolder> {

    static final int FIRST = 1;
    static final int DEFAULT = 2;

    private IAppCache mAppCache;

    @Inject
    public WeatherAdapter(IAppCache appCache) {
        mAppCache = appCache;
        setHasStableIds(true);
    }

    public interface OnItemClickListener {
        void onWeatherItemClicked(WeatherSimpleModel model);
    }

    private OnItemClickListener mWeatherItemClickListener;
    private List<WeatherSimpleModel> mWeatherData = new Vector<>();

    public void setWeatherItemClickListener(OnItemClickListener listener) {
        mWeatherItemClickListener = listener;
    }

    public void setWeatherData(List<WeatherSimpleModel> weatherList) {
        mWeatherData.clear();
        mWeatherData.addAll(weatherList);
        notifyDataSetChanged();
    }

    public void addNewWeatherInfo(WeatherSimpleModel model) {
        if (!containsId(model.getId())) {
            mWeatherData.add(0, model);
            notifyItemInserted(1);
        }
    }

    private boolean containsId(long id) {
        for (int i = 0; i < mWeatherData.size(); i++) {
            if (mWeatherData.get(i).getId() == id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? FIRST : DEFAULT;
    }

    @Override
    public long getItemId(int position) {
        return mWeatherData.get(position).getId();
    }

    @Override
    public WeatherDefaultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        WeatherDefaultViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case FIRST:
                view = inflater.inflate(R.layout.view_weather_item_first, parent, false);
                viewHolder = new WeatherFirstViewHolder(view);
                break;
            case DEFAULT:
                view = inflater.inflate(R.layout.view_weather_item, parent, false);
                viewHolder = new WeatherDefaultViewHolder(view);
                break;
            default:
                throw new IllegalArgumentException("Wrong viewHolder viewType");
        }
        view.setOnClickListener(v -> mWeatherItemClickListener.onWeatherItemClicked((WeatherSimpleModel) v.getTag()));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(WeatherDefaultViewHolder holder, int position) {
        WeatherSimpleModel curModel = mWeatherData.get(position);
        Context context = holder.itemView.getContext();

        holder.tempView.setText(getTemperature(context.getResources(), curModel.getTemp()));
        holder.timeView.setText(curModel.getRequestTime());
        holder.cityView.setText(curModel.getCity());
        holder.itemView.setTag(curModel);
        GlideApp.with(holder.iconView)
                .load(curModel.getIconUrl())
                .error(R.drawable.ic_error_black_24dp)
                .into(holder.iconView);

        //holder.iconView
        if (holder instanceof WeatherFirstViewHolder) {
            ((WeatherFirstViewHolder) holder).descriptionView.setText(curModel.getDescription());
        }
    }

    private String getTemperature(Resources res, double temperature) {
        switch (mAppCache.getTemperaturePreference()) {
            case IAppCache.PREF_CELSIUS:
                return res.getString(R.string.celsius_degrees, OtherUtils.convertToCelsius(temperature));
            case IAppCache.PREF_FAHRENHEIT:
                return res.getString(R.string.fahrenheit_degrees, OtherUtils.convertToFahrenheit(temperature));
            default:
                return temperature + "";
        }
    }

    @Override
    public int getItemCount() {
        return mWeatherData == null ? 0 : mWeatherData.size();
    }

    static class WeatherDefaultViewHolder extends RecyclerView.ViewHolder {
        TextView tempView;
        TextView cityView;
        TextView timeView;
        ImageView iconView;

        WeatherDefaultViewHolder(View v) {
            super(v);
            tempView = v.findViewById(R.id.tv_temp);
            cityView = v.findViewById(R.id.tv_city);
            timeView = v.findViewById(R.id.tv_time);
            iconView = v.findViewById(R.id.ic_weather);
        }
    }

    static class WeatherFirstViewHolder extends WeatherDefaultViewHolder {
        TextView descriptionView;

        WeatherFirstViewHolder(View v) {
            super(v);
            descriptionView = v.findViewById(R.id.tv_desc);
        }
    }
}