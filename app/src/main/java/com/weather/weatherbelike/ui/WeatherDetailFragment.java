package com.weather.weatherbelike.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weather.weatherbelike.R;
import com.weather.weatherbelike.ui.model.WeatherDetailModel;

public class WeatherDetailFragment extends Fragment {

    public static WeatherDetailFragment newInstance() {
        return new WeatherDetailFragment();
    }

    private WeatherViewModel mWeatherViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWeatherViewModel = ViewModelProviders.of(getActivity()).get(WeatherViewModel.class);
    }

    private TextView mTempView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_weather_detail, container, false);
        mTempView = view.findViewById(R.id.tempView);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mWeatherViewModel.getSelectedItem().observe(this, new Observer<WeatherDetailModel>() {
            @Override
            public void onChanged(@Nullable WeatherDetailModel weatherDetailModel) {
                mTempView.setText(weatherDetailModel.toString());
            }
        });
    }
}
