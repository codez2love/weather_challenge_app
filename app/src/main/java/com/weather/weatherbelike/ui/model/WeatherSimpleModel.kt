package com.weather.weatherbelike.ui.model

import com.weather.weatherbelike.data.extensions.empty

data class WeatherSimpleModel(val id: Long,
                              val temp: Double,
                              val city: String,
                              val requestTime: String,
                              val description: String,
                              val iconUrl: String) {

    companion object {
        fun empty() = WeatherSimpleModel(0, Double.MIN_VALUE, String.empty(),
                String.empty(), String.empty(), String.empty())
    }
}