package com.weather.weatherbelike.ui;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.weather.weatherbelike.domain.GetNewWeatherInfo;
import com.weather.weatherbelike.domain.GetWeatherItemDetails;
import com.weather.weatherbelike.domain.GetWeatherRequestHistory;
import com.weather.weatherbelike.ui.model.WeatherDetailModel;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

public class WeatherViewModel extends ViewModel {
    private final MutableLiveData<WeatherDetailModel> mSelectedItem
            = new MutableLiveData<>();

    private final MutableLiveData<String> mFailures = new MutableLiveData<>();
    private final MutableLiveData<WeatherSimpleModel> mLastItem = new MutableLiveData<>();

    private MutableLiveData<List<WeatherSimpleModel>> mListData;


    private GetWeatherRequestHistory mGetRequestHistoryUC;
    private GetWeatherItemDetails mGetWeatherItemDetailsUC;
    private GetNewWeatherInfo mGetNewWeatherInfoUC;

    @Inject
    public WeatherViewModel(GetWeatherRequestHistory getRequestHistoryUC,
                            GetWeatherItemDetails getWeatherItemDetailsUC,
                            GetNewWeatherInfo getNewWeatherInfoUC) {
        mGetNewWeatherInfoUC = getNewWeatherInfoUC;
        mGetWeatherItemDetailsUC = getWeatherItemDetailsUC;
        mGetRequestHistoryUC = getRequestHistoryUC;
    }

    public void fetchNewInfo() {
        mGetNewWeatherInfoUC.execute(new DisposableObserver<WeatherSimpleModel>() {
            @Override
            public void onNext(WeatherSimpleModel simpleModel) {
                mLastItem.setValue(simpleModel);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                mFailures.setValue("Failed to fetch data from API");
            }

            @Override
            public void onComplete() {
                Log.i("WeatherViewModel", "GetNewWeatherInfoUC: onComplete()");
            }
        }, null);
    }

    public void selectItem(WeatherSimpleModel simpleModel) {
        mGetWeatherItemDetailsUC.execute(new DisposableObserver<WeatherDetailModel>() {
            @Override
            public void onNext(WeatherDetailModel weatherDetailModel) {
                mSelectedItem.setValue(weatherDetailModel);
            }

            @Override
            public void onError(Throwable e) {
                mFailures.setValue("Failed to get details from DB");
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                Log.i("WeatherViewModel", "GetWeatherItemDetailsUC: onComplete()");
            }
        }, simpleModel.getId());
    }

    public MutableLiveData<WeatherDetailModel> getSelectedItem() {
        return mSelectedItem;
    }

    public MutableLiveData<String> getExceptions() {
        return mFailures;
    }

    public MutableLiveData<WeatherSimpleModel> getLastItem() {
        return mLastItem;
    }

    public MutableLiveData<List<WeatherSimpleModel>> getListData() {
        if (mListData == null) {
            mListData = new MutableLiveData<>();
            mGetRequestHistoryUC.execute(new DisposableObserver<List<WeatherSimpleModel>>() {
                @Override
                public void onNext(List<WeatherSimpleModel> weatherSimpleModels) {
                    mListData.setValue(weatherSimpleModels);
                }

                @Override
                public void onError(Throwable e) {
                    mFailures.setValue("Failed to fetch data from DB");
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {
                    Log.i("WeatherViewModel", "GetRequestHistoryUC: onComplete()");
                }
            }, null);
        }
        return mListData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mGetRequestHistoryUC.dispose();
        mGetWeatherItemDetailsUC.dispose();
        mGetNewWeatherInfoUC.dispose();
    }
}
