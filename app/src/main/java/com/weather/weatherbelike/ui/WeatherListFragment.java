package com.weather.weatherbelike.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.weather.weatherbelike.R;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class WeatherListFragment extends Fragment {

    public static WeatherListFragment newInstance() {
        return new WeatherListFragment();
    }

    @Inject
    WeatherAdapter mRecAdapter;
    private WeatherViewModel mWeatherViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        mWeatherViewModel = ViewModelProviders.of(getActivity()).get(WeatherViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_weather_list, container, false);
        RecyclerView recView = view.findViewById(R.id.recView);
        recView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setStackFromEnd(false);
        recView.setLayoutManager(manager);
        recView.setAdapter(mRecAdapter);


        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_fall_down);
        recView.setLayoutAnimation(animation);

        mRecAdapter.setWeatherItemClickListener(
                model -> mWeatherViewModel.selectItem(model));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mWeatherViewModel.getListData().observe(this,
                weatherSimpleModels -> mRecAdapter.setWeatherData(weatherSimpleModels));

        mWeatherViewModel.getLastItem().observe(this,
                weatherSimpleModel -> mRecAdapter.addNewWeatherInfo(weatherSimpleModel));
    }
}
