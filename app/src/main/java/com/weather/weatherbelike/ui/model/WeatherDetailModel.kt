package com.weather.weatherbelike.ui.model

import com.weather.weatherbelike.data.extensions.empty

data class WeatherDetailModel(val minTemp: Double,
                              val maxTemp: Double,
                              val curTemp: Double,
                              val description: String,
                              val pressure: String,
                              val humidity: String,
                              val requestTime: String,
                              val City: String,
                              val iconUrl: String) {
    companion object {
        fun empty() = WeatherDetailModel(Double.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE, String.empty(),
                String.empty(), String.empty(), String.empty(), String.empty(), String.empty())
    }
}


