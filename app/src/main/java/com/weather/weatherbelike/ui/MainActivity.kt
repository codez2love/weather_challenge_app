package com.weather.weatherbelike.ui

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.weather.weatherbelike.R
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import permissions.dispatcher.*
import javax.inject.Inject

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) =
        beginTransaction().func().commit()

@RuntimePermissions
class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var mDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    lateinit var mWeatherViewModel: WeatherViewModel

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        mWeatherViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(WeatherViewModel::class.java)

        setContentView(R.layout.activity_main)
        addFragment(savedInstanceState)

        mWeatherViewModel.exceptions.observe(this, Observer {
            Toast.makeText(super@MainActivity.getApplicationContext(), it, Toast.LENGTH_SHORT).show()
        })

        mWeatherViewModel.selectedItem.observe(this, Observer {
            Log.d("MainActivity", "Item clicked")
            // showDetailFragment()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == R.id.menu_get_new) {
            fetchNewWeatherInfoWithPermissionCheck()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun addFragment(savedInstanceState: Bundle?) =
            savedInstanceState ?: supportFragmentManager.inTransaction {
                add(R.id.list_frag, WeatherListFragment.newInstance())
            }


    private fun showDetailFragment() =
            supportFragmentManager.inTransaction {
                add(R.id.list_frag, WeatherDetailFragment.newInstance())
                        .addToBackStack("")

            }


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return mDispatchingAndroidInjector
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @NeedsPermission(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun fetchNewWeatherInfo() {
        mWeatherViewModel.fetchNewInfo()
    }

    @OnShowRationale(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun onRationaleForLocation(request: PermissionRequest) {
        AlertDialog.Builder(this)
                .setMessage("Allow use of location, please!")
                .setPositiveButton("allow", { dialog, button -> request.proceed() })
                .setNegativeButton("deny", { dialog, button -> request.cancel() })
                .show()
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun onDeniedForLocation() {
        Toast.makeText(this, "User pressed never ask again", Toast.LENGTH_LONG).show();
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun onNeverAskForLocation() {
        Toast.makeText(this, "User pressed never ask again", Toast.LENGTH_LONG).show();
    }
}

