package com.weather.weatherbelike.domain;

import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.UIThread;
import com.weather.weatherbelike.ui.model.WeatherDetailModel;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetLatestWeatherRequest extends UseCase<WeatherDetailModel, Void> {

    private IWeatherRepo mWeatherRepo;

    @Inject
    public GetLatestWeatherRequest(ThreadExecutor executor,
                                   UIThread uiThread,
                                   IWeatherRepo weatherRepo) {
        super(executor, uiThread);
        mWeatherRepo = weatherRepo;
    }

    @Override
    protected Observable<WeatherDetailModel> buildUseCaseObservable(Void aVoid) {
        return mWeatherRepo.getLatestWeatherRequest().toObservable();
    }
}
