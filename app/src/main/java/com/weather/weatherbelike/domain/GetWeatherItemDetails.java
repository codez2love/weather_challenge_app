package com.weather.weatherbelike.domain;

import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.UIThread;
import com.weather.weatherbelike.ui.model.WeatherDetailModel;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetWeatherItemDetails extends UseCase<WeatherDetailModel, Long> {

    private IWeatherRepo mWeatherRepo;

    @Inject
    public GetWeatherItemDetails(ThreadExecutor executor,
                                 UIThread uiThread,
                                 IWeatherRepo weatherRepo) {
        super(executor, uiThread);
        mWeatherRepo = weatherRepo;
    }

    @Override
    protected Observable<WeatherDetailModel> buildUseCaseObservable(Long integer) {
        return mWeatherRepo.getWeatherItemDetails(integer).toObservable();
    }
}
