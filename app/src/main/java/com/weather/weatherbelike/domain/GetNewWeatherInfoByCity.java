package com.weather.weatherbelike.domain;

import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.UIThread;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetNewWeatherInfoByCity extends UseCase<WeatherSimpleModel, String> {

    private IWeatherRepo mWeatherRepo;

    @Inject
    public GetNewWeatherInfoByCity(ThreadExecutor executor,
                                   UIThread uiThread,
                                   IWeatherRepo weatherRepository) {
        super(executor, uiThread);
        mWeatherRepo = weatherRepository;
    }

    @Override
    protected Observable<WeatherSimpleModel> buildUseCaseObservable(String cityInput) {
        return mWeatherRepo.getNewWeatherInfo(cityInput).toObservable();
    }
}
