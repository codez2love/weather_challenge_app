package com.weather.weatherbelike.domain;

import com.weather.weatherbelike.ui.model.WeatherDetailModel;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface IWeatherRepo {

    Single<WeatherSimpleModel> getNewWeatherInfo(String city);

    Single<WeatherSimpleModel> getNewWeatherInfo(double lat, double lon);

    Single<WeatherDetailModel> getWeatherItemDetails(Long id);

    Flowable<List<WeatherSimpleModel>> getWeatherRequestHistory();

    Single<WeatherDetailModel> getLatestWeatherRequest();
}
