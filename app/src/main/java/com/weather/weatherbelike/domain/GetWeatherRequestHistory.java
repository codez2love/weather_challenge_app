package com.weather.weatherbelike.domain;

import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.UIThread;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetWeatherRequestHistory extends UseCase<List<WeatherSimpleModel>, Void> {

    private IWeatherRepo mWeatherRepo;

    @Inject
    public GetWeatherRequestHistory(ThreadExecutor executor,
                                    UIThread uiThread,
                                    IWeatherRepo repo) {
        super(executor, uiThread);
        mWeatherRepo = repo;
    }

    @Override
    protected Observable<List<WeatherSimpleModel>> buildUseCaseObservable(Void aVoid) {
        return mWeatherRepo.getWeatherRequestHistory().toObservable();
    }
}
