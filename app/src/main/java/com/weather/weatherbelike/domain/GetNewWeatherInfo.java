package com.weather.weatherbelike.domain;

import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.UIThread;
import com.weather.weatherbelike.data.cache.IAppCache;
import com.weather.weatherbelike.data.tools.ILocationProvider;
import com.weather.weatherbelike.ui.model.WeatherSimpleModel;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class GetNewWeatherInfo extends UseCase<WeatherSimpleModel, Void> {

    private IWeatherRepo mWeatherRepo;
    private ILocationProvider mLocProvider;
    private IAppCache mAppCache;

    @Inject
    public GetNewWeatherInfo(ThreadExecutor executor,
                             UIThread uiThread,
                             IWeatherRepo weatherRepository,
                             ILocationProvider locationProvider,
                             IAppCache appCache) {
        super(executor, uiThread);
        mWeatherRepo = weatherRepository;
        mLocProvider = locationProvider;
        mAppCache = appCache;
    }

    @Override
    protected Observable<WeatherSimpleModel> buildUseCaseObservable(Void aVoid) {
        return Observable.just(mAppCache.getDefaultCity())
                .flatMapSingle(city -> {
                    if (city.equals("default")) {
                        return mLocProvider.getLastKnownLocation()
                                .flatMap(location ->
                                        mWeatherRepo.getNewWeatherInfo(location.getLatitude(), location.getLongitude())
                                                .subscribeOn(Schedulers.from(threadExecutor)))
                                .map(weatherSimpleModel -> {
                                    mAppCache.setDefaultCity(weatherSimpleModel.getCity());
                                    return weatherSimpleModel;
                                });
                    } else {
                        return mWeatherRepo.getNewWeatherInfo(city);
                    }
                });
    }
}
