package com.weather.weatherbelike;

import android.os.Build;

import com.facebook.stetho.Stetho;
import com.weather.weatherbelike.di.AppModule;
import com.weather.weatherbelike.di.DaggerApplicationComponent;
import com.weather.weatherbelike.di.RoomModule;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class WeatherApp extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            if (!Build.FINGERPRINT.equals("robolectric")) {
                Stetho.initializeWithDefaults(this);
            }
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent
                .builder()
                .application(this)
                .appModule(new AppModule(this))
                .roomModule(new RoomModule(this))
                .build();
    }
}
