package com.weather.weatherbelike.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.weather.weatherbelike.ui.WeatherViewModel;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import dagger.Subcomponent;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private final HashMap<Class, Callable<? extends ViewModel>> creators;

    @Inject
    public ViewModelFactory(final ViewModelSubComponent viewModelSubComponent) {
        creators = new HashMap<>();

        // View models cannot be injected directly because they won't be bound to the owner's view model scope.
        creators.put(WeatherViewModel.class, new Callable<ViewModel>() {
            @Override
            public ViewModel call() throws Exception {
                return viewModelSubComponent.weatherViewModel();
            }
        });
        //creators.put(ProjectListViewModel.class, () -> viewModelSubComponent.projectListViewModel());
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        Callable<? extends ViewModel> creator = creators.get(modelClass);
        if (creator == null) {
            for (Map.Entry<Class, Callable<? extends ViewModel>> entry : creators.entrySet()) {
                if (modelClass.isAssignableFrom(entry.getKey())) {
                    creator = entry.getValue();
                    break;
                }
            }
        }
        if (creator == null) {
            throw new IllegalArgumentException("Unknown model class " + modelClass);
        }
        try {
            return (T) creator.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Subcomponent
    public interface ViewModelSubComponent {
        @Subcomponent.Builder
        interface Builder {
            ViewModelSubComponent build();
        }

        WeatherViewModel weatherViewModel();
    }

}
