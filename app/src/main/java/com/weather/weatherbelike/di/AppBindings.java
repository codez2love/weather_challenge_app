package com.weather.weatherbelike.di;

import com.weather.weatherbelike.data.database.WeatherContentProvider;
import com.weather.weatherbelike.ui.MainActivity;
import com.weather.weatherbelike.ui.WeatherDetailFragment;
import com.weather.weatherbelike.ui.WeatherListFragment;
import com.weather.weatherbelike.ui.widget.WeatherWidgetProvider;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AppBindings {

    @ContributesAndroidInjector
    abstract WeatherContentProvider weatherContentProvider();

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector
    abstract WeatherWidgetProvider widgetProvider();

    @Module
    static abstract class MainActivityModule {

        @ContributesAndroidInjector
        abstract WeatherListFragment weatherListFragment();

        @ContributesAndroidInjector
        abstract WeatherDetailFragment weatherDetailFragment();
    }
}
