package com.weather.weatherbelike.di;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.content.res.Resources;

import com.weather.weatherbelike.JobExecutor;
import com.weather.weatherbelike.data.ThreadExecutor;
import com.weather.weatherbelike.data.cache.IAppCache;
import com.weather.weatherbelike.data.cache.MainCache;
import com.weather.weatherbelike.data.repository.WeatherRepository;
import com.weather.weatherbelike.data.repository.local.IWeatherLocalStore;
import com.weather.weatherbelike.data.repository.local.IWeatherRoomStore;
import com.weather.weatherbelike.data.repository.local.WeatherLocalStore;
import com.weather.weatherbelike.data.tools.ILocationProvider;
import com.weather.weatherbelike.data.tools.LocationProvider;
import com.weather.weatherbelike.data.utils.DisplayDateFormatter;
import com.weather.weatherbelike.data.utils.IDateFormatter;
import com.weather.weatherbelike.domain.IWeatherRepo;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(subcomponents = ViewModelFactory.ViewModelSubComponent.class)
public class AppModule {

    private Context mAppContext;

    public AppModule(Context context) {
        mAppContext = context;
    }

    @Provides
    Context provideAppContext() {
        return mAppContext;
    }

    @Provides
    Resources provideAndroidResources() {
        return mAppContext.getResources();
    }

    @Provides
    ILocationProvider provideLocationProvider() {
        return new LocationProvider(mAppContext);
    }

    @Provides
    static ThreadExecutor provideThreadExecutor(JobExecutor executor) {
        return executor;
    }

    @Provides
    static IWeatherRepo provideWeatherRepo(WeatherRepository repository) {
        return repository;
    }

    @Provides
    static IDateFormatter provideDateFormatter(DisplayDateFormatter formatter) {
        return formatter;
    }

    @Provides
    static IWeatherLocalStore provideWeatherLocalStore(WeatherLocalStore localStore) {
        return localStore;
    }

    @Provides
    static IWeatherRoomStore provideRoomStore(WeatherLocalStore localStore) {
        return localStore;
    }

    @Provides
    static IAppCache provideAppCache(MainCache cache) {
        return cache;
    }

    @Singleton
    @Provides
    static ViewModelProvider.Factory provideViewModelFactory(
            ViewModelFactory.ViewModelSubComponent.Builder viewModelSubComponent) {
        return new ViewModelFactory(viewModelSubComponent.build());
    }

}
