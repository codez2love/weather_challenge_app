package com.weather.weatherbelike.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.weather.weatherbelike.data.database.AppDatabase;
import com.weather.weatherbelike.data.database.WeatherDao;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private AppDatabase mAppDatabase;

    public RoomModule(Context appContext) {
        mAppDatabase = Room
                .databaseBuilder(appContext, AppDatabase.class, "weather_db")
                //.addMigrations(AppDatabase.MIGRATION_1_2)
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    WeatherDao provideWeatherDao() {
        return mAppDatabase.weatherDao();
    }
}
