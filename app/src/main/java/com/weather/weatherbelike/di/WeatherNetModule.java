package com.weather.weatherbelike.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.weather.weatherbelike.data.api.WeatherApis;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public abstract class WeatherNetModule {

    public static final String API_URL = "https://api.openweathermap.org/";
    public static final String IMG_URL = "https://openweathermap.org/img/w/";
    static final String API_KEY = "628f09e8e6e43ec32222d0063e1873d4";

    @Provides
    @Named("ApiEndpoint")
    static String provideAppUrl() {
        return API_URL;
    }

    @Provides
    @Named("APPID")
    static String provideApiKey() {
        return API_KEY;
    }

    @Provides
    static Gson provideGsonConverter() {
        return new GsonBuilder()
                .create();
    }

    @Provides
    static HttpLoggingInterceptor provideInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    static OkHttpClient provideClient(HttpLoggingInterceptor interceptor) {
        OkHttpClient client;
        client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();
        return client;
    }

    @Provides
    @Singleton
    static Retrofit provideRetrofit(OkHttpClient client,
                                    @Named("ApiEndpoint") String apiUrl,
                                    Gson converter) {
        return new Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(converter))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    static WeatherApis provideWeatherApis(Retrofit retrofit) {
        return retrofit.create(WeatherApis.class);
    }


}
