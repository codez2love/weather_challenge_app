package com.weather.weatherbelike.di;

import android.app.Application;

import com.weather.weatherbelike.WeatherApp;
import com.weather.weatherbelike.ui.widget.WeatherWidgetProvider;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class,
        AppModule.class,
        RoomModule.class,
        AppBindings.class,
        WeatherNetModule.class
})
public interface ApplicationComponent extends AndroidInjector<WeatherApp> {
    void inject(WeatherApp app);

    void inject(WeatherWidgetProvider widget);


    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application app);

        Builder appModule(AppModule appModule);

        Builder roomModule(RoomModule module);

        ApplicationComponent build();
    }
}
